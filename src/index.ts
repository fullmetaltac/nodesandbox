import pg from "pg";
import express from "express";

const app = express();

const connectionString = 'postgresql://postgres:postgres@postgres:5432/postgres'
const client = new pg.Client(connectionString);
client.connect();

app.get("/", (req, res) => {
  client.query('SELECT * FROM phones;', (qerr, qres) => {
    if(qres){
      res.send(qres.rows[0]);
    }
    else{
      res.send('No phones!');
    }
    client.end()
  })
});

const server = app.listen(process.env.PORT, () => {
  console.log("Started at http://localhost:%d\n", server.address().port);
});

// CREATE TABLE phones (
//   ID serial PRIMARY KEY,
//   name VARCHAR (255) NOT NULL
//  );
//  SELECT * FROM phones;
//  INSERT INTO phones (name) VALUES ('Pixel2');